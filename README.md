THE THING
---------

a web project created by Lena Daxenbichler
for the course "programmierakademie berufsbegleitend basis" at BFI Innsbruck
2020-2021

Status
------
This project is dormant, but might be revisited.
Code fixes need to be implemented (don't mind the console errors!)
The "finished" result is found on branch "dev".

Programming
-----------
Languages/Techniques Used:
- HTML
- CSS
- JQuery
- JavaScript
- php
- SQL

Server Side
-----------
"the thing" is hosted locally via xampp.
connection to the mysql database is required.

Client 
------
JavaScript is used for certain actions. Browser compatibility has not been tested extensively.

Adding new "things"
-------------------
To add a new "thing", supply the img url (in /img) with the desired ID to the database (phpmyadmin).
To add new specs (circles), supply position, size, image url and text data to the db / img (don't forget the thing id!).
Supply images in the img/ folder (file names need to be correct).
New circles will automatically be spawned based on the number of objects in the database.
The variable "id" has to be changed in index.php order to reflect changes (not implemented).
Watch for file names in folders.

Minigame
--------
Click the strings to play them!
The minigame is implemented as an iframe to work around the database, an approach i wanted to take to
improve and learn new skills within project limitations - e.g. adding new content without changing the
main code, or index.php .

Updates
-------
This page was last updated at 25.01.2021
New content might be added sporadically.

Personal use
------------
Feel free to use any code or segments in your personal projects (if you don't mind the sloppy code)!
If using a larger segment, please link to this repository.
