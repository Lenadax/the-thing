<?php

//open connection
function OpenCon()
 {
 $dbhost = "localhost";
 $dbuser = "lena";
 $dbpass = "123";
 $db = "dasding";
 $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);
 
 return $conn;
 }
 
function CloseCon($conn)
 {
 $conn -> close();
 }

 $conn = OpenCon();



//generate ID
$ID = 1;


//select data of the thing
$sql = "SELECT * FROM dinge
WHERE ding_ID=$ID";
$result = $conn->query($sql);

while($row = $result->fetch_assoc()) {
  $name = $row["ding_name"];
  $img = $row["ding_img"];
}

//create class
class HTMLDiv
{
    public $name;
    public $descr;
    public $pos_top;
    public $pos_left;
    public $imgurl;
    public $img_nr;
    public $width;
    public $height;
    public $hwidth;
    public $hheight;

    public function __construct(string $name, string $descr, string $pos_top, string $pos_left, string $imgurl, string $img_nr, string $width, string $height, string $hwidth, string $hheight)
    {
        $this->name = $name;
        $this->descr= $descr;
        $this->pos_top= $pos_top;
        $this->pos_left = $pos_left;
        $this->imgurl= $imgurl;
        $this->img_nr = $img_nr;
        $this->height = $height;
        $this->width = $width;
        $this->hwidth = $hwidth;
        $this->hheight = $hheight;
    }

}

$divArray = [];

//SQL
for ($i=0; $i<10; $i++) { 
  $sql = "SELECT * FROM imgs
  WHERE ding_ID=$ID AND img_nr=$i";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()) {
    array_push($divArray, new HTMLDiv($row["name"], $row["descr"], $row["pos_top"], $row["pos_left"], $row["imgurl"], $row["img_nr"], $row["width"],  $row["height"], $row["hheight"], $row["hwidth"]));
  }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>



<body>

<!--homepage-->
<div id="wrapper">
<h1 id="thething">
    <span id="hl1">the thing</span>
    <span id="hl2">
        <?php echo $name; ?>
    </span>
    <br>
    <button id="enter-button">&darr;</button>
</h1>

    <!--content-->
    <div id="contentwrapper">

        <div id="innercontent">
            <img id="object" src="<?php echo $img; ?>">
        </div>

        <?php
        foreach ($divArray as $divData) {
          print "
          <div id='spec$divData->img_nr' class='objcircle'>
            <!--hover effect-->
            <style>
            #spec$divData->img_nr {
              width:$divData->width;
              height:$divData->height;
              position:absolute;
              top:$divData->pos_top;
              left:$divData->pos_left;
            }
            #spec$divData->img_nr:hover {
              background-image: url($divData->imgurl);
              width:$divData->hwidth;
              height:$divData->hheight;
              box-shadow:0 0 .5rem black;
              position:absolute;
            }
            </style>
            <div id='specwrap$divData->img_nr' class='spwrap'>
              <div class='specs' id='specs$divData->img_nr'>
                <h3>$divData->name</h3>
                <span>$divData->descr</span>
              </div>

              <div class='spec-circ' id='spec-circ$divData->img_nr'>
                <style>
                #spec-circ$divData->img_nr {background-image: url($divData->imgurl);}
                </style>
              </div>
            </div>
          </div>
          ";
        }
        ?>

      <div class="footer">
        <span type="button" id="contact">Contact
          <br>
          <div id="contactinfo">
            <span>
              <h5>Lena Daxenbichler</h5>
              <br>
              <a href="#">Website</a>
              <br>
              <a href="#">Contact me</a>
            </span>
        </div>
      </span>  
    </div>
  </div>  
</div>


    
</body>


<script src="script.js"></script>
</html>