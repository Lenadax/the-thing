

function spawnParticles(){
    let wrapper = document.getElementById("wrapper")
    
    for(i=0; i<20; i++){
        let elem = document.createElement("div")
        elem.classList.add("particle")
        wrapper.appendChild(elem)
        console.log("reeeeee")
        elem.style.left = Math.random() * 100 + "%"
        elem.style.top = "-10px"
        elem.style.opacity = Math.random()
        elem.style.animation = "snowAnim " + (5 + Math.random() * 8) + "s"
        elem.style.animationDelay = (-Math.random() * 10) + "s"
        elem.style.animationIterationCount = "infinite"
        elem.style.animationTimingFunction = "linear"
    }
}

spawnParticles();


button = document.getElementById("enter-button")

button.addEventListener("click", () => {
    let h1 = document.getElementById("thething")
    let cwrap = document.getElementById("contentwrapper")
    /*hides button, moves headline*/
    h1.style.top = "15%"
    h1.style.fontSize = "3rem"
    button.style.opacity = "0"
    /*shows content on click*/
    cwrap.style.display = "flex"
    cwrap.style.animation = "2s fadeIn"
    // set display properties of title
    let hl1 = document.getElementById("hl1")
    let hl2 = document.getElementById("hl2")
    hl1.style.display = "none"
    hl2.style.display = "block"
})


//show contact info

function showContact() {
    let btn = document.getElementById("contact")
    let cinfo = document.getElementById("contactinfo")

    btn.addEventListener("click", event => {
        console.log("clicked contact")
        cinfo.style.display = "block"
    })

    btn.addEventListener("mouseleave", event => {
        console.log("left contact")
        cinfo.style.display = "none"
    })
}

showContact();




//show specs
function showSpecs(){
    for(i=0; i<10; i++){
        let spec = document.getElementById("spec" + i)
        let specwrap = document.getElementById("specwrap" + i)

        //show specs on click
        spec.addEventListener("click", event => {
            // show specs
            console.log("it worked!" + i)
            specwrap.style.display = "block"
        })
        spec.addEventListener("mouseleave", event => {
            //remove on mouseleave
            specwrap.style.display = "none"
        })
    }
}
showSpecs();

